import spacy


def should_skip_token(t, nlp):
    lemma = t.lemma_
    if nlp.is_stop(lemma) != 0:
        return True
    # if not t.is_alpha:
    #     return True
    return False


def normalize_text(txt, nlp, lemmatization, lower):
    txt = nlp(txt)
    normalized = list()
    for word in txt:
        if not should_skip_token(word, nlp):
            if lemmatization:
                val = word.lemma_.strip()
                normalized.append(val)
            else:
                val = word.text.strip()
                if lower:
                    val = val.lower()
                normalized.append(val)
    return " ".join(normalized)


def normalize_text_df(df, col):
    nlp = spacy.load("en")
    df['normalized'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, False, False))
    df['normalized_lemmatized'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, True, False))
    df['normalized_lower'] = df[col].progress_apply(lambda s: normalize_text(s, nlp, False, True))

