import re
import string

import numpy as np
import pandas as pd
from nltk.corpus import stopwords
from tqdm import tqdm

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)
tqdm.pandas()

from readability_index import *

stop_words = stopwords.words('english')

FEATURES = [
    'num_smpl_punctuation',
    'num_symbols',
    'num_unique_words',
    'punct_num',
    'punct_freq',
    'lower_num',
    'lower_freq',
    'upper_num',
    'upper_freq',
    'len_char',
    'len_word',
    'avg_word_len',
    'len_word_expt_stop',
    'sentence_count',
    'avg_sentence_length',
    'syllables_count',
    'avg_syllables_per_word',
    'difficult_words',
    'poly_syllable_count',
    'flesch_reading_ease',
    'gunning_fog',
    'smog_index',
    'dale_chall_readability_score'
]


def word_len(s):
    return len(s.split())


def word_len_except_stop(s):
    s = set(s.split())
    count = 0
    for w in s:
        if w not in stop_words:
            count += 1

    return count


def avg_word_len(s):
    ss = s.split()
    if len(ss) == 0:
        return 0
    else:
        return np.mean([len(x) for x in ss])


def num_of_regex_occurences(regexp, text):
    return len(re.findall(regexp, text))


def add_custom_features(df, normalized_col, original_col):
    df['len_char'] = df[normalized_col].progress_apply(len)
    df['len_word'] = df[normalized_col].progress_apply(word_len)
    df['avg_word_len'] = df[normalized_col].progress_apply(avg_word_len)
    df['len_word_expt_stop'] = df[normalized_col].progress_apply(word_len_except_stop)

    col = 'num_smpl_punctuation'
    df[col] = df[original_col].apply(lambda comment: sum(comment.count(w) for w in '.,;:'))

    col = 'num_symbols'
    df[col] = df[original_col].apply(lambda comment: sum(comment.count(w) for w in '*&$%'))

    col = 'num_unique_words'
    df[col] = df[original_col].apply(lambda comment: len(set(w for w in comment.split())))

    col = 'punct_num'
    punct_pattern = '[{}]'.format(string.punctuation)
    df[col] = df[original_col].progress_apply(lambda s: num_of_regex_occurences(punct_pattern, s))

    col = 'punct_freq'
    df[col] = df[original_col].progress_apply(lambda s: (1.0 * num_of_regex_occurences(punct_pattern, s)) / len(s))

    col = 'lower_num'
    lower_pattern = '[a-z]'
    df[col] = df[original_col].progress_apply(lambda s: num_of_regex_occurences(lower_pattern, s))

    col = 'lower_freq'
    df[col] = df[original_col].progress_apply(lambda s: (1.0 * num_of_regex_occurences(lower_pattern, s)) / len(s))

    col = 'upper_num'
    upper_pattern = '[A-Z]'
    df[col] = df[original_col].progress_apply(lambda s: num_of_regex_occurences(upper_pattern, s))

    col = 'upper_freq'
    df[col] = df[original_col].progress_apply(lambda s: (1.0 * num_of_regex_occurences(upper_pattern, s)) / len(s))

    df['sentence_count'] = df[normalized_col].progress_apply(sentence_count)
    df['avg_sentence_length'] = df[normalized_col].progress_apply(avg_sentence_length)
    df['syllables_count'] = df[normalized_col].progress_apply(syllables_count)
    df['avg_syllables_per_word'] = df[normalized_col].progress_apply(avg_syllables_per_word)
    df['difficult_words'] = df[normalized_col].progress_apply(difficult_words)
    df['poly_syllable_count'] = df[normalized_col].progress_apply(poly_syllable_count)
    df['flesch_reading_ease'] = df[normalized_col].progress_apply(flesch_reading_ease)
    df['gunning_fog'] = df[normalized_col].progress_apply(gunning_fog)
    df['smog_index'] = df[normalized_col].progress_apply(smog_index)
    df['dale_chall_readability_score'] = df[normalized_col].progress_apply(dale_chall_readability_score)


def load_hilary_vs_donald_normalized():
    hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'
    return pd.read_csv(hilary_vs_donald_fp)


if __name__ == '__main__':
    df = load_hilary_vs_donald_normalized()
    add_custom_features(df, 'normalized', 'tweet')
