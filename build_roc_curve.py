import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, auc

hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'
test_results_fp = 'predictions/ensemble.csv'


def load_hilary_vs_donald_normalized():
    return pd.read_csv(hilary_vs_donald_fp)


def split_into_train_val_test(df):
    val = '2017-06-02'
    tr = '2017-06-01'

    def get_split_label(s):
        if s <= tr:
            return 'train'
        if s <= val:
            return 'validation'
        return 'test'

    df['split'] = df['date'].apply(get_split_label)

    train = df[df.split == 'train']
    validation = df[df.split == 'validation']
    test = df[df.split == 'test']

    return train, validation, test


def find_best_cutoff_and_accuracy(y_true, probs):
    s = np.sort(probs)
    cutoffs = [(s[i] + s[i + 1]) / 2 for i in range(len(s) - 2)]
    best_cuttoff = None
    best_accuracy = -1
    for c in cutoffs:
        predictions = (probs > c).astype(int)
        accuracy = sum(predictions.values == y_true) / len(probs)
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_cuttoff = c

    return best_cuttoff, best_accuracy


if __name__ == '__main__':
    preds = pd.read_csv(test_results_fp).realDonaldTrump

    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)
    y_test = test.target

    fpr, tpr, threshold = roc_curve(y_test, preds)
    roc_auc = auc(fpr, tpr)

    best_cuttoff, best_accuracy = find_best_cutoff_and_accuracy(y_test, preds)

    plt.title('Receiver Operating Characteristic ')
    label = f'AUC={roc_auc:0.3f} (Accuracy={best_accuracy:0.3f})'
    plt.plot(fpr, tpr, 'b', label=label)
    plt.legend(loc='lower right')
    plt.plot([0, 1], [0, 1], 'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()
