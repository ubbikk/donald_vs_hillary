import sys
import warnings

import lightgbm as lgb
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score, accuracy_score

from preprocesing_for_baselines import *

if not sys.warnoptions:
    warnings.simplefilter("ignore")

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'


def load_hilary_vs_donald_normalized():
    return pd.read_csv(hilary_vs_donald_fp)


def split_into_train_val_test(df):
    val = '2017-06-02'
    tr = '2017-06-01'

    def get_split_label(s):
        if s <= tr:
            return 'train'
        if s <= val:
            return 'validation'
        return 'test'

    df['split'] = df['date'].apply(get_split_label)

    train = df[df.split == 'train']
    validation = df[df.split == 'validation']
    test = df[df.split == 'test']

    return train, validation, test


def _run_naive_bayes_svm_baseline(train, test, validation, target, field, max_features):
    data_train, data_val, data_test = preprocess_for_nb(train, validation, test, target, field, max_features)
    target_train, target_val, target_test = train[target], validation[target], test[target]
    model = LogisticRegression(C=1, dual=True)
    model.fit(data_train, target_train)

    probs_train = model.predict_proba(data_train)[:, 1]
    probs_val = model.predict_proba(data_val)[:, 1]
    probs_test = model.predict_proba(data_test)[:, 1]

    auc_train = roc_auc_score(target_train, probs_train)
    auc_val = roc_auc_score(target_val, probs_val)
    auc_test = roc_auc_score(target_test, probs_test)

    acc_train = accuracy_score(target_train, probs_train > 0.5)
    acc_val = accuracy_score(target_val, probs_val > 0.5)
    acc_test = accuracy_score(target_test, probs_test > 0.5)

    # print(f'Train AUC {auc_train}')
    # print(f'Val  AUC  {auc_val}')
    # print(f'Test  AUC {auc_test}')

    # print(f'Train Accuracy {acc_train}')
    # print(f'Val  Accuracy  {acc_val}')
    # print(f'Test  Accuracy {acc_test}')

    return probs_train, probs_val, probs_test, {'test_auc':auc_test}


def _run_linear_baseline(train, test, validation, target, field):
    data_train, data_val, data_test = preprocess_for_linear_mixed_word_char(train, validation, test, field)
    target_train, target_val, target_test = train[target], validation[target], test[target]
    model = LogisticRegression(C=1, n_jobs=4)
    model.fit(data_train, target_train)

    probs_train = model.predict_proba(data_train)[:, 1]
    probs_val = model.predict_proba(data_val)[:, 1]
    probs_test = model.predict_proba(data_test)[:, 1]

    auc_train = roc_auc_score(target_train, probs_train)
    auc_val = roc_auc_score(target_val, probs_val)
    auc_test = roc_auc_score(target_test, probs_test)

    acc_train = accuracy_score(target_train, probs_train > 0.5)
    acc_val = accuracy_score(target_val, probs_val > 0.5)
    acc_test = accuracy_score(target_test, probs_test > 0.5)

    # print(f'Train AUC {auc_train}')
    # print(f'Val  AUC  {auc_val}')
    # print(f'Test  AUC {auc_test}')

    # print(f'Train Accuracy {acc_train}')
    # print(f'Val  Accuracy  {acc_val}')
    # print(f'Test  Accuracy {acc_test}')
    return probs_train, probs_val, probs_test, {'test_auc':auc_test}


def _run_lgbm_baseline(train, test, validation, target, field):
    data_train, data_val, data_test = preprocess_for_lgbm_mixed_word_char(train, validation, test, field)
    target_train, target_val, target_test = train[target], validation[target], test[target]
    watchlist = [
        lgb.Dataset(data_train, target_train),
        lgb.Dataset(data_test, target_test)
    ]
    params = {
        "objective": "binary",
        'metric': {'auc'},
        "boosting_type": "gbdt",
        "verbosity": -1,
        "num_threads": 4,
        "bagging_fraction": 0.8,
        "feature_fraction": 0.8,
        "learning_rate": 0.1,
        "num_leaves": 31,
        "verbose": -1,
        "min_split_gain": .1,
        "reg_alpha": .1,
        "num_boost_round": 500,
        "early_stopping_rounds": 50
    }
    model = lgb.train(
        params=params,
        train_set=watchlist[0],
        num_boost_round=500,
        valid_sets=watchlist,
        early_stopping_rounds=30,
        verbose_eval=0
    )

    probs_train = model.predict(data_train, num_iteration=model.best_iteration)
    probs_val = model.predict(data_val, num_iteration=model.best_iteration)
    probs_test = model.predict(data_test, num_iteration=model.best_iteration)

    auc_train = roc_auc_score(target_train, probs_train)
    auc_val = roc_auc_score(target_val, probs_val)
    auc_test = roc_auc_score(target_test, probs_test)

    acc_train = accuracy_score(target_train, probs_train > 0.5)
    acc_val = accuracy_score(target_val, probs_val > 0.5)
    acc_test = accuracy_score(target_test, probs_test > 0.5)

    # print(f'Train AUC {auc_train}')
    # print(f'Val  AUC  {auc_val}')
    # print(f'Test  AUC {auc_test}')

    # print(f'Train Accuracy {acc_train}')
    # print(f'Val  Accuracy  {acc_val}')
    # print(f'Test  Accuracy {acc_test}')

    return probs_train, probs_val, probs_test, {'test_auc':auc_test}


def run_naive_bayes_svm_baseline(df, field, max_features=4000):
    train, validation, test = split_into_train_val_test(df)
    target = 'target'
    return _run_naive_bayes_svm_baseline(train, test, validation, target, field, max_features)


def run_linear_baseline(df, field):
    train, validation, test = split_into_train_val_test(df)
    target = 'target'
    return _run_linear_baseline(train, test, validation, target, field)


def run_lgbm_baseline(df, field):
    train, validation, test = split_into_train_val_test(df)
    target = 'target'
    return _run_lgbm_baseline(train, test, validation, target, field)


if __name__ == '__main__':
    # ROC_AUC
    # for naive_bayes_svm_baseline: 0.8865569721404961
    # ROC_AUC
    # for lgbm_baseline: 0.8744233772837892
    # ROC_AUC
    # for linear_baseline: 0.8695143656242055

    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)
    col = 'tweet'

    probs_train, probs_val, probs_test, res = run_naive_bayes_svm_baseline(df, col)
    auc = res['test_auc']
    print(f'ROC_AUC for naive_bayes_svm_baseline: {auc}')
    test['realDonaldTrump']=probs_test
    test['HillaryClinton'] = 1-test['realDonaldTrump']
    result_df = test[['id', 'realDonaldTrump', 'HillaryClinton']]
    result_df.to_csv('predictions/naive_bayes_svm_baseline.csv', index=False)

    probs_train, probs_val, probs_test, res = run_lgbm_baseline(df, col)
    auc = res['test_auc']
    print(f'ROC_AUC for lgbm_baseline: {auc}')
    test['realDonaldTrump']=probs_test
    test['HillaryClinton'] = 1-test['realDonaldTrump']
    result_df = test[['id', 'realDonaldTrump', 'HillaryClinton']]
    result_df.to_csv('predictions/lgbm_baseline.csv', index=False)

    probs_train, probs_val, probs_test, res = run_linear_baseline(df, col)
    auc = res['test_auc']
    print(f'ROC_AUC for linear_baseline: {auc}')
    test['realDonaldTrump']=probs_test
    test['HillaryClinton'] = 1-test['realDonaldTrump']
    result_df = test[['id', 'realDonaldTrump', 'HillaryClinton']]
    result_df.to_csv('predictions/linear_baseline.csv', index=False)
