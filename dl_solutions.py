import pandas as pd
from keras.layers import Bidirectional, GlobalMaxPool1D
from keras.layers import Dense, Input, LSTM,GRU, Embedding, Dropout, concatenate
from keras.models import Model
from keras.optimizers import Adam, SGD
from keras.callbacks import LearningRateScheduler

from keras_callbacks import SimpleRocAucCallback
from preprocesing_for_dl import *

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'

simple_fastext_fp = '../../data/fasttext/wiki.simple.vec'
target = 'target'


def load_hilary_vs_donald_normalized():
    return pd.read_csv(hilary_vs_donald_fp)


def split_into_train_val_test(df):
    val = '2017-06-02'
    tr = '2017-06-01'

    def get_split_label(s):
        if s <= tr:
            return 'train'
        if s <= val:
            return 'validation'
        return 'test'

    df['split'] = df['date'].apply(get_split_label)

    train = df[df.split == 'train']
    validation = df[df.split == 'validation']
    test = df[df.split == 'test']

    return train, validation, test


def get_model_lstm(maxlen, max_features, embed_size, embedding_matrix):
    inp = Input(shape=(maxlen,))
    x = Embedding(max_features, embed_size, weights=[embedding_matrix], trainable=False)(inp)
    x = Bidirectional(GRU(50, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))(x)
    x = GlobalMaxPool1D()(x)
    x = Dense(50, activation="relu")(x)
    x = Dropout(0.3)(x)
    x = Dense(1, activation="sigmoid")(x)
    model = Model(inputs=inp, outputs=x)
    model.compile(loss='binary_crossentropy', optimizer=Adam(1e-3), metrics=['acc'])

    return model


def get_model_lstm_with_custom_features(maxlen, max_features, embed_size, embedding_matrix):
    embeddings_input = Input(shape=(maxlen,))
    features_input = Input(shape=(23,))

    x = Embedding(max_features, embed_size, weights=[embedding_matrix], trainable=False)(embeddings_input)
    x = Bidirectional(GRU(50, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))(x)
    x = GlobalMaxPool1D()(x)
    x = Dense(50, activation="relu")(x)
    x = Dropout(0.2)(x)

    y = Dense(10)(features_input)
    y = Dropout(0.4)(y)

    z = concatenate([x, y])
    x = Dense(1, activation="sigmoid")(z)

    model = Model(inputs=[embeddings_input, features_input], outputs=x)
    model.compile(loss='binary_crossentropy', optimizer=Adam(1e-3), metrics=['acc'])

    return model


def run_lstm_with_custom_features(col):
    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)
    max_len = 200
    max_features = 10_000
    embed_size = 300
    target_train, target_val, target_test = train[target], validation[target], test[target]
    emb_train_data, emb_validation_data, emb_test_data, embedding_matrix = preprocess_for_dl(train,
                                                                                             validation,
                                                                                             test,
                                                                                             col,
                                                                                             simple_fastext_fp,
                                                                                             max_len=max_len,
                                                                                             max_features=max_features,
                                                                                             embed_size=embed_size,
                                                                                             fasttext=True,
                                                                                             init_with_zero=True)
    feat_train, feat_val, feat_test = preprocess_basic_features(train, validation, test)
    train_data = [emb_train_data, feat_train]
    validation_data = [emb_validation_data, feat_val]
    test_data = [emb_test_data, feat_test]
    roc = SimpleRocAucCallback((train_data, target_train), (test_data, target_test))
    model = get_model_lstm_with_custom_features(max_len, max_features, embed_size, embedding_matrix)
    h = model.fit(train_data, target_train,
                  validation_data=(test_data, target_test),
                  epochs=5, callbacks=[roc], batch_size=256)

def run_simple_gru(col):
    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)
    max_len=200
    max_features=10_000
    embed_size=300
    target_train, target_val, target_test = train[target], validation[target], test[target]
    train_data, validation_data, test_data, embedding_matrix = preprocess_for_dl(train,
                                                                                 validation,
                                                                                 test,
                                                                                 col,
                                                                                 simple_fastext_fp,
                                                                                 max_len=max_len,
                                                                                 max_features=max_features,
                                                                                 embed_size=embed_size,
                                                                                 fasttext=True,
                                                                                 init_with_zero=True)

    roc = SimpleRocAucCallback((train_data, target_train), (test_data, target_test))
    def schedule(epoch, lr):
        arr = [1e-3]*3+[1e-4]*10
        new_lr = arr[epoch]
        print(f'# {epoch}: current lr is {lr} -> {new_lr}')
        return new_lr

    sch = LearningRateScheduler(schedule)
    model = get_model_lstm(max_len, max_features, embed_size, embedding_matrix)
    h = model.fit(train_data, target_train,
                  validation_data=(test_data, target_test),
                  epochs=5, callbacks=[roc, sch], batch_size=256)


if __name__ == '__main__':
    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)
