1. Data(train\test) was obtained via scraping of Twitter 


    I used twint (https://github.com/haccer/twint)
    
    Training dataset contains approximately 20000 tweets,
    it consists of all tweets in the period from 2015 till 2017-06-01
    
    Test dataset contains approximately 2500 tweets 
    and it consists of all tweets after 2017-06-01
    
    All data(train + test) is in hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv
    
    Code for scrapping is in scrape_tweets.py
    
    Code for creating train\test dataset is in creating_dataset.py

2. So my validation scheme, in fact, is OOT (Out-Of-Time)

	I selected it in order to prevent leakage of information 	from the future. 
	Although we will see worse scores (compared to random train\test split) such scheme is more appropriate for real-world tasks. 

3. I choose AUC-ROC as my evaluation metric, because of its relative robustness to class imbalance

4. I trained a few baseline classificators - in order to understand situation better:
    
    You can find implementation in baselines.py
    
    
    a)Modification of 'Baselines and Bigrams: Simple, Good Sentiment and Topic Classification'
    (http://www.aclweb.org/anthology/P12-2018)
    It achieved 88.7% ROC-AUC
    
    b)LightGbm + word and char ngram features
    It achieved 87.4% ROC-AUC
    
    c) Logistic Regression + word and char ngram features
    It achieved 87% ROC-AUC
    
5. As a solution I trained a few Deep Learning Models:

    
    a)Simple LSTM + some (spatial + ordinary) dropout.

    I used the last fasttext for creating an embedding layer.

    Also, I averaged predictions across several epochs - it provided a slight improvement.

    It achieved 92.7% ROC-AUC

    b)Simple GRU + some (spatial + ordinary) + some additional, hand-crafted features.

    I used the last fasttext for creating an embedding layer.

    Additional features are:

    many 'statistics' about text number\frequency of punctuation, average token length etc.

    Also, I used 'readability features' i.e. features that measure 'difficulty of text'.

    (I borrowed idea and implementation from this page https://www.geeksforgeeks.org/readability-index-pythonnlp/)

    You may find the calculation of these features in extract_basic_features.py and in readability_index.py

    These model turned out to be the best:

    it achieved 93% ROC-AUC i.e. it slightly outperformed 'straightforward' DL solution.
    
    The corresponding Accuracy is approximately 94%
    
6. I tried kaggle-like ensembling:
 
 
    weight average 1x for baselines 2x for more powerfull DL-models.
    It achieved 94% ROC-AUC - i.e. 1% gain.
    The corresponding Accuracy is approximately 94.5%
    
    You can find the code for ensembling in ensempling.py
    
7. You may find implementation of DL models + code for training in 

 
    j_try_gru_with_custom_features.ipynb and 
    j_try_simple_lstm.ipynb
    
8. Preprocessing:


    It consisted of only tokenization followed by lemmatization.

    I used spaCy.

    Code for text preprocessing is in text_normalization.py.
    
9. Predictions of the best model(and other models as well) are in predictions/

10. The plot of ROC AUC curve is in roc_auc.png

11. Further steps. I had little time to work on this task so I want to share some ideas that can help improve quality:

    1. Tune parameters of DL models. Current is a rough guess.

    2. Use some sort of learning rate annealing,

    3. Use more powerful language model (instead of plain word embedding)

    For instance realization of 'Regularizing and Optimizing LSTM Language Models'(https://arxiv.org/pdf/1708.02182.pdf)

    4. Try implement ideas from this paper: https://arxiv.org/abs/1801.06146

    (https://arxiv.org/abs/1801.06146)
    (Fine tuning of language model)
