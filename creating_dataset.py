import numpy as np
import pandas as pd
from tqdm import tqdm

from extract_basic_features import add_custom_features
from text_normalization import normalize_text_df

tqdm.pandas()

hilary_fp = 'hilary_vs_donald_data/hilary.csv'
donald_fp = 'hilary_vs_donald_data/trump_after_2015.csv'
hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'


def shuffle_df(df, random_state=241):
    np.random.seed(random_state)
    return df.iloc[np.random.permutation(len(df))]


def load_hilary_vs_donald():
    h = pd.read_csv(hilary_fp)
    h['target'] = 0
    d = pd.read_csv(donald_fp)
    d['target'] = 1
    df = pd.concat([h, d])
    return shuffle_df(df)


if __name__ == '__main__':
    df = load_hilary_vs_donald()
    df = df[['id', 'date', 'tweet', 'target']]
    normalize_text_df(df, 'tweet')
    add_custom_features(df, 'normalized_lower', 'tweet')
    df.to_csv(hilary_vs_donald_fp, index=False)
