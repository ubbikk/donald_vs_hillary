import numpy as np
from scipy.sparse import csr_matrix, hstack
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm

tqdm.pandas()


def exctract_features_two_vectorizers(df, vec1, vec2, field):
    f1 = vec1.transform(df[field])
    f2 = vec2.transform(df[field])

    return hstack([f1, f2])


def preprocess_for_linear_mixed_word_char(train, validation, test, field):
    word_vectorizer = TfidfVectorizer(
        sublinear_tf=True,
        strip_accents='unicode',
        analyzer='word',
        token_pattern=r'\w{1,}',
        stop_words='english',
        ngram_range=(1, 1),
        max_features=2000)

    char_vectorizer = TfidfVectorizer(
        sublinear_tf=True,
        strip_accents='unicode',
        analyzer='char',
        stop_words='english',
        ngram_range=(2, 6),
        max_features=2000)

    word_vectorizer.fit(train[field])
    char_vectorizer.fit(train[field])

    data_train = exctract_features_two_vectorizers(train, word_vectorizer, char_vectorizer, field)
    data_validation = exctract_features_two_vectorizers(validation, word_vectorizer, char_vectorizer, field)
    data_test = exctract_features_two_vectorizers(test, word_vectorizer, char_vectorizer, field)

    return data_train, data_validation, data_test


def char_analyzer_lgbm(text):
    tokens = text.split()
    return [token[i: i + 3] for token in tokens for i in range(len(token) - 2)]


def preprocess_for_lgbm_mixed_word_char(train, valdation, test, field):
    ngram_vectorizer = TfidfVectorizer(
        sublinear_tf=True,
        strip_accents='unicode',
        analyzer='word',
        token_pattern=r'\w{1,}',
        stop_words='english',
        ngram_range=(1, 2),
        max_features=2000)

    word_vectorizer = TfidfVectorizer(
        sublinear_tf=True,
        strip_accents='unicode',
        tokenizer=char_analyzer_lgbm,
        analyzer='word',
        ngram_range=(1, 1),
        max_features=2000)

    ngram_vectorizer.fit(train[field])
    word_vectorizer.fit(train[field])

    data_train = exctract_features_two_vectorizers(train, ngram_vectorizer, word_vectorizer, field)
    data_validation = exctract_features_two_vectorizers(valdation, ngram_vectorizer, word_vectorizer, field)
    data_test = exctract_features_two_vectorizers(test, ngram_vectorizer, word_vectorizer, field)

    return data_train, data_validation, data_test


def prior(train, target_series, target_val):
    p = train[target_series == target_val].sum(axis=0)
    d = (target_series == target_val).sum()

    return (p + 1) / (d + 1)


def exctract_features_nb(train, validation, test, vec, target, field):
    data_train = vec.transform(train[field])
    data_val = vec.transform(validation[field])
    data_test = vec.transform(test[field])

    r_train = np.log(prior(data_train, train[target].values, 1) / prior(data_train, train[target].values, 0))
    r_train = csr_matrix(r_train)
    data_train = data_train.multiply(r_train)

    data_test = data_test.multiply(r_train)
    data_val = data_val.multiply(r_train)

    return data_train, data_val, data_test


def preprocess_for_nb(train, validation, test, target, field, max_features):
    vec = TfidfVectorizer(ngram_range=(1, 2), token_pattern=r'\w{1,}',
                          min_df=3, max_df=0.9, strip_accents='unicode', use_idf=1,
                          smooth_idf=1, sublinear_tf=1, max_features=max_features, lowercase=True)

    vec.fit_transform(train[field])

    data_train, data_val, data_test = exctract_features_nb(train, validation, test, vec, target, field)

    return data_train, data_val, data_test
