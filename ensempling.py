import sys
import warnings

import pandas as pd
from sklearn.metrics import roc_auc_score

if not sys.warnoptions:
    warnings.simplefilter("ignore")

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_colwidth', 5000)

hilary_vs_donald_fp = 'hilary_vs_donald_data/hilary_vs_donald_after_2015_normalized.csv'


def load_hilary_vs_donald_normalized():
    return pd.read_csv(hilary_vs_donald_fp)


def split_into_train_val_test(df):
    val = '2017-06-02'
    tr = '2017-06-01'

    def get_split_label(s):
        if s <= tr:
            return 'train'
        if s <= val:
            return 'validation'
        return 'test'

    df['split'] = df['date'].apply(get_split_label)

    train = df[df.split == 'train']
    validation = df[df.split == 'validation']
    test = df[df.split == 'test']

    return train, validation, test

if __name__ == '__main__':
    df = load_hilary_vs_donald_normalized()
    train, validation, test = split_into_train_val_test(df)

    fps = [
        'predictions/lgbm_baseline.csv',
        'predictions/linear_baseline.csv',
              'predictions/naive_bayes_svm_baseline.csv'
          ] + \
          2 * [
        'predictions/simple_lstm_solution.csv',
              'predictions/gru_with_custom_features_solution.csv',
    ]

    df = sum([pd.read_csv(fp, index_col='id') for fp in fps])/len(fps)
    print(df[:10])

    probs = df['realDonaldTrump'].values

    auc = roc_auc_score(test.target, probs)
    print(f'Roc AUC of simple average is {auc}')

    test['realDonaldTrump']=probs
    print(test['realDonaldTrump'].describe())
    test['HillaryClinton'] = 1-test['realDonaldTrump']
    result_df = test[['id', 'realDonaldTrump', 'HillaryClinton']]
    result_df.to_csv('predictions/ensemble.csv', index=False)
